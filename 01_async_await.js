const fs = require("fs");

// const nodejsProcess = this.process;

function fetchData() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Math.random() < 0.2) {
                return reject(new Error('fetch error'));
            }
            resolve({ fake: 'data' });
        }, 1000)
    });
}


function processData(data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Math.random() < 0.2) {
                return reject(new Error('process error'))
            }
            resolve({ fake: 'processed' + data.fake });
        }, 1000)
    });
}


async function process() {
    const data = await fetchData();
    return processData(data);
}


async function main() {
    const data = await process();
    try {
        fs.writeFileSync("out.json", JSON.stringify(data));
        console.log("Done");
    } catch (e) {
        console.error("Error writing data", e);
    }
}


main().catch(err => console.error("Error processing data", err));
