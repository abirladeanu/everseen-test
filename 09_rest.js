const http = require("http");
const cluster = require("cluster");

const { iteratorParse } = require("./07_iterator_parse");
const throttle = require("./10_throttle");

const MAX_REQUESTS = 10;


async function parseLogs(body, res) {
    await new Promise(resolve => {
        setTimeout(resolve, 10000);
    });
    // ideally i would validate the input matches what i expect (otherwise 400), but i'm kind of in a hurry
    const data = JSON.parse(body);
    const files = data.f;
    const levels = data.l;
    const filters = data.s;

    for (const file of files) {
        for await (const s of iteratorParse(file, levels, filters)) {
            s.file = file;
            res.write(JSON.stringify(s));
            res.write("\n");
        }
    }
    // for scaling I would use cluster.fork(), send the data to the client, then onMessage from worker i would send the message to the response

    res.end();
}


function requestListener(req, res) {
    if (!throttle.requestCanStart()) {
        res.writeHead(503);
        res.end("Service at full capacity");
        return;
    }

    if (req.method === "POST") {
        let body = "";
        req.on("data", (chunk) => body += chunk);
        req.on("end", () => {
            console.log("Request", req.url, req.method, body);

            if (req.url === "/logparser") {
                parseLogs(body, res).catch(err => {
                    console.error(err);
                    res.writeHead(500);
                    res.end("Internal server error");
                });
            } else {
                res.writeHead(404);
                res.end("Not found");
            }
        });
    } else if (req.method === "GET") {
        console.log("Request", req.url, req.method, req.data);
        res.writeHead(200);
        res.end("Hello world");
    } else {
        res.writeHead(404);
        res.end("Not found");
    }

    res.on("finish", () => {
        console.log("Response finished");
        throttle.endRequest();
    })
}

async function main() {
    if (cluster.isMaster) {
        throttle.setMaxRequests(MAX_REQUESTS)
        const server = http.createServer(requestListener);
        server.listen(5000, () => {
            console.log("Server is running");
        });
    } else {

    }
}

if (require.main === module) {
    main().catch(err => console.error(err));
}

module.exports = {
    main,
}
