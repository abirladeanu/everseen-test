const mocha = require("mocha");
const {assert} = require("chai");
const {matchesFilter} = require("./07_iterator_parse");

describe("Tests", function () {

    it ("matches regex filters", () => {
        const log = {
            date: "2020-01-02",
            time: "01:02:03.500",
            tz: "+05:00",
            level: "INFO",
            message: "This is an INFO message"
        };

        const result = matchesFilter(log, ["info"], [/info/i])
        assert.isTrue(result);
    })
});