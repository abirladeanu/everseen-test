FROM node:14-slim

WORKDIR /work

EXPOSE 5000

COPY . .

ENTRYPOINT ["node", "13_delivery.js"]
CMD ["run-as-server"]

