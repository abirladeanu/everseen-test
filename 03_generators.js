
// the output is
// 4
// 3
// 2
// 1
// 0


function* counter(count, reset_times) {
    for (let idx = 0; idx < reset_times; idx++) {
        let currentCount = count;
        while (currentCount--) yield (count - currentCount - 1);
    }
}

for (const v of counter(5, 2)) {
    console.log(v);
}
