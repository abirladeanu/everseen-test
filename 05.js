

String.prototype.add = function() {
    const numbers = this.split(",").map(x => parseInt(x)).filter(x => !isNaN(x));
    return numbers.reduce((acc, current) => acc + current, 0);
}

String.prototype.mul = function() {
    const numbers = this.split(",").map(x => parseInt(x)).filter(x => !isNaN(x));
    return numbers.reduce((acc, current) => acc * current, 1);
}

console.log("1,2,3,4,a,6".add());
console.log("1,2,3,4".mul());
