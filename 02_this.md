
`console.log(f())` outputs 10 because, since it's not inside an object, the `this` keyword refers to the global context, inside which p is 10.
`console.log(x.f())` outputs 5 because `this` keyword refers to the object `x`
`console.log(y.f())` outputs 20 because the `this` keyword refers to the object `y`

If `p=10` is deleted, the first console outputs `undefined`, the others remain the same.
