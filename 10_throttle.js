
let maxRequests = 0;
let requestInProgress = 0;


function setMaxRequests(reqs) {
    maxRequests = reqs;
}

function requestCanStart() {
    if (requestInProgress < maxRequests) {
        requestInProgress++;
        return true;
    }
    return false;
}

function endRequest() {
    requestInProgress--;
}


module.exports = {
    setMaxRequests,
    requestCanStart,
    endRequest,
}