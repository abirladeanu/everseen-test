const { iteratorParse } = require("./07_iterator_parse");

/**
 *
 * @param cmd {string}
 */
function processCommandLine(cmd) {
    const files = [];
    const levels = [];
    const regexes = [];

    const fileRegex = /-f ([a-zA-Z.\-]*)/g;
    const levelRegex = /-l (\w+)/g;
    const regexRegex = /-s (\w+)/g;

    for (const data of cmd.matchAll(fileRegex)) {
        files.push(data[1]);
    }

    for (const data of cmd.matchAll(levelRegex)) {
        levels.push(data[1]);
    }

    for (const data of cmd.matchAll(regexRegex)) {
        regexes.push(new RegExp(data[1], "i"));
    }

    return { files, levels, regexes };
}

/**
 *
 * @param files {string[]}
 * @param levels {string[]}
 * @param filters {RegExp[]}
 * @returns {Promise<void>}
 */
async function parseFiles(files, levels, filters) {
    // another option would be to start a check on every file and then do a Promise.allSettled,
    // but i thought it would make more sense to go through files one by one
    for (const file of files) {
        for await (const s of iteratorParse(file, levels, filters)) {
            s.file = file;
            console.log(s);
        }
    }
}

async function main() {
    const cmd = process.argv.join(" ");
    console.log("Command line", cmd);
    const {files, levels, regexes} = processCommandLine(cmd);
    await parseFiles(files, levels, regexes);
}

module.exports = {
    parseFiles,
    main,
}

if (require.main === module) {
    main().catch(err => console.error(err));
}
