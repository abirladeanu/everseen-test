const testLog = `
2020-01-02 01:02:03.100+05:00 DEBUG This is a dummy DEBUG message

2020-01-03 01:02:03.200-05:00 INFO This is a random INFO message
2020-01-04 01:02:03.300+05:00 ERROR This is an test ERROR message
2020-01-05 01:02:03.400+05:00 INFO This is a another random INFO message
2020-01-06 01:02:03.500+05:00 ERROR This is an different test ERROR message
2020-01-07 01:02:03.600+05:00 DEBUG This is a dummy test DEBUG message
`

/**
 *
 * @param log {string}
 */
function parseLogs(log) {
    const parsedLogs = [];
    const re = /([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3})(\+|-)([0-9]{2}:[0-9]{2}) (\w*) (.*)/g;

    for (const data of log.matchAll(re)) {
        parsedLogs.push({
            date: data[1],
            time: data[2],
            tz: `${data[3]}${data[4]}`,
            level: data[5],
            message: data[6]
        });
    }

    return parsedLogs;

}

// console.log(parseLogs(testLog));

module.exports = {
    parseLogs
}