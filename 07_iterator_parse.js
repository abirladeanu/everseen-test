const fs = require("fs");
const { promisify } = require("util");

const openFile = promisify(fs.open);
const readFile = promisify(fs.read)

const READ_CHUNK = 100;

function* enhancedParseLogs(log) {
    const re = /([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3})(\+|-)([0-9]{2}:[0-9]{2}) (\w*) (.*)/g;


    for (const data of log.matchAll(re)) {
        const parsedLog = {
            date: data[1],
            time: data[2],
            tz: `${data[3]}${data[4]}`,
            level: data[5],
            message: data[6]
        };
        const index = data.index;
        const length = data[0].length;
        yield { parsedLog, index, length };
    }
}

/**
 *
 * @param data {{date: string, time: string, tz: string, level: string, message: string}}
 * @param levels {string[]}
 * @param filters {RegExp[]}
 * @returns {boolean}
 */
function matchesFilter(data, levels, filters) {
    if (!levels.includes(data.level.toLowerCase())) {
        return false;
    }

    return filters.some(re => data.message.match(re));
}



/**
 *
 * @param logFile {string}
 * @param levels {string[]}
 * @param filters {RegExp[]}
 */
async function* iteratorParse(logFile, levels, filters) {
    if (!fs.existsSync(logFile)) {
        throw new Error("Log file doesn't exist");
    }

    levels = levels.map(x => x.toLowerCase());

    const fileHandler = await openFile(logFile, "r");
    const buffer = Buffer.alloc(READ_CHUNK);
    let bytesRead = READ_CHUNK;
    let fileOffset = 0;
    // the last message might be invalid / incomplete, ideally i would handle it properly
    while (bytesRead === READ_CHUNK) {
        const readResult = await readFile(fileHandler, buffer, 0, READ_CHUNK, fileOffset);
        bytesRead = readResult.bytesRead;
        let lastIndex = 0;
        let lastLength = 0;
        for (const { parsedLog, index, length } of enhancedParseLogs(buffer.toString("utf8"))) {
            if (matchesFilter(parsedLog, levels, filters)) {
                yield parsedLog;
            }
            lastIndex = index;
            lastLength = length;
        }

        fileOffset += lastIndex + lastLength;
    }
}


async function main() {
    for await (const s of iteratorParse("log.log", [], [])) {
        console.log(s);
    }
}

module.exports = {
    matchesFilter,
    iteratorParse
}

// main().catch(err => console.error(err));